import React, { useEffect } from 'react';
import * as AuthRequest from '@/api/AuthRequest';

const Login = () => {
  useEffect(() => {
    AuthRequest.login({
      email: 'test1@mailinator.com',
      password: '123456',
    }).then(data => console.log('Login: ', data));
  }, []);

  return <div>Login Page</div>;
};

export default Login;
