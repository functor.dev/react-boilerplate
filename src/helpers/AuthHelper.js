import Cookies from 'universal-cookie';
import { path, pathOr, pick, pipe } from 'ramda';

export function storeAuthToken(info) {
  const cookies = new Cookies();
  const options = {
    path: '/',
    maxAge: info.token.expires_in,
    // secure: true,
    // httpOnly: true,
  };
  const token = path(['token', 'access_token'], info);
  const userId = path(['result'], info);
  const userInfo = pipe(
    pathOr({}, ['entities', 'registerInfo', String(userId)]),
    pick(['email', 'id', 'is_admin', 'is_verify', 'name', 'phone', 'roles']),
  )(info);
  cookies.set('token', token, options);
  cookies.set('userInfo', JSON.stringify(userInfo), options);
}

export function removeAuthToken() {
  const cookies = new Cookies();
  cookies.remove('token', {
    path: '/',
  });
  cookies.remove('userInfo', {
    path: '/',
  });
}
