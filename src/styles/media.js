/*
 * Media queries utility
 */

import { css } from 'styled-components/macro';

/*
 * Taken from https://github.com/DefinitelyTyped/DefinitelyTyped/issues/32914
 */

// Update your breakpoints if you want
export const sizes = {
  xxs: 480,
  xs: 576,
  sm: 768,
  md: 992,
  lg: 1200,
  xl: 1360,
  xxl: 1440,
};

// Iterate through the sizes and create a media template
const baseMedia = query =>
  Object.keys(sizes).reduce((acc, label) => {
    acc[label] = (first, ...interpolations) => css`
      @media (${query}: ${sizes[label]}px) {
        ${css(first, ...interpolations)}
      }
    `;

    return acc;
  }, {});

const media = {
  down: baseMedia('max-width'),
  up: baseMedia('min-width'),
};

export default media;
