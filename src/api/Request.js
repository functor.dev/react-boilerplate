import axios from 'axios';
import { always, cond, has, mergeLeft, pathOr, pipe } from 'ramda';
import Cookies from 'universal-cookie';
import { store } from '@/index';
import { API_URL } from '@/config';
import { Actions as AuthActions } from '@/reducers/auth';
import axiosRetry from 'axios-retry';

const cookies = new Cookies();

const axiosInstance = axios.create({
  baseURL: API_URL,
});

axiosRetry(axiosInstance, {
  retries: 3,
});

axiosInstance.interceptors.request.use(
  config => {
    const customConfig = pathOr({}, ['customConfig'], config);
    const token = cookies.get('token');
    const headers =
      token && !customConfig.noToken
        ? {
            Authorization: `Bearer ${token}`,
          }
        : {};

    return {
      ...config,
      headers: {
        ...config.headers,
        ...headers,
      },
    };
  },
  error => Promise.reject(error),
);

axiosInstance.interceptors.response.use(
  response => response.data,
  error => {
    const statusCode = pathOr('XXX', ['response', 'status'], error);

    if (statusCode === 408 || error.code === 'ECONNABORTED') {
      return Promise.reject(error);
    }

    if (axios.isCancel(error)) {
      return Promise.reject(error);
    }

    const data = pipe(
      cond([
        [has('response'), pathOr({}, ['response', 'data'])],
        [has('request'), pathOr({}, ['request'])],
        [always(true), always(error)],
      ]),
      mergeLeft({
        statusCode,
        fromAxiosInterceptors: true,
      }),
    )(error);
    const customConfig = pathOr({}, ['config', 'customConfig'], error);

    if (statusCode === 401 && !customConfig.noRedirect401) {
      // NOTE: setTimeout to have time finish async task on caller
      setTimeout(() => {
        store.dispatch(AuthActions.logout());
      }, 100);
    }

    if (statusCode === 403) {
    }

    if (statusCode === 404 && !customConfig.noRedirect404) {
    }

    if (statusCode === 500) {
    }

    return Promise.reject(data);
  },
);

export const request = (
  config = {
    noToken: false,
    noRedirect404: false,
  },
) => {
  const normalizeConfig = {
    ...config,
    noToken: config.noToken || false,
    noRedirect404: config.noRedirect404 || false,
  };

  return {
    get(url, params = {}, options = {}) {
      return axiosInstance.get(url, {
        ...options,
        customConfig: normalizeConfig,
        params,
      });
    },

    post(url, data, options = {}) {
      return axiosInstance.post(url, data, {
        ...options,
        customConfig: normalizeConfig,
      });
    },

    put(url, data, options = {}) {
      return axiosInstance.put(url, data, {
        ...options,
        customConfig: normalizeConfig,
      });
    },

    delete(url, data, options = {}) {
      return axiosInstance.request({
        ...options,
        // @ts-ignore
        customConfig: normalizeConfig,
        method: 'delete',
        url,
        data,
      });

      // return axiosInstance.delete(url, {
      //   ...options,
      //   customConfig: normalizeConfig,
      //   data,
      // });
    },

    custom(config) {
      return axiosInstance(config);
    },

    getInstance() {
      return axiosInstance;
    },
  };
};

export default request;
