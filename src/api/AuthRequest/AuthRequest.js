import Request from '../Request';

export const register = data => {
  return Request().post('/auth/register', data);
};

export const login = data => {
  return Request().post('/auth/login', data);
};
