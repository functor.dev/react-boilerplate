export const isObject = v =>
  Object.prototype.toString.call(v) === '[object Object]';
