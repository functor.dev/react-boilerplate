import { curry, hasPath } from 'ramda';

export const unwrapActionResult = curry((action, result) => {
  if (!hasPath(['fulfilled', 'match'], action)) {
    return Promise.reject();
  }

  return action.fulfilled.match(result)
    ? Promise.resolve(result)
    : Promise.reject(result);
});
