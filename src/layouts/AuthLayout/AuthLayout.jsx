import React from 'react';
import PropTypes from 'prop-types';

import { renderRoutes } from 'react-router-config';

import * as Styled from './styled';

const AuthLayout = ({ route }) => {
  return (
    <Styled.GuestLayout>
      <Styled.Background image="https://source.unsplash.com/random/1600x900" />

      <Styled.Main>{renderRoutes(route.routes)}</Styled.Main>
    </Styled.GuestLayout>
  );
};

AuthLayout.propTypes = {
  route: PropTypes.object.isRequired,
};

export default AuthLayout;
