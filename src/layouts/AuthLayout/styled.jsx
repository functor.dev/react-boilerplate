import styled from 'styled-components';

export const GuestLayout = styled.div`
  display: flex;
  min-height: 100vh;
  position: relative;
`;

export const Background = styled.div`
  background: rgba(0, 0, 0, 0.1) url(${props => props.image}) no-repeat center;
  background-size: cover;
  bottom: 0;
  left: 0;
  position: absolute;
  top: 0;
  right: 0;
`;

export const Main = styled.div`
  width: 100%;
  position: relative;
  z-index: 10;
`;
