import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import Cookies from 'universal-cookie';

import makeRootReducer from '@/redux/reducers';
import rootSaga from './rootSaga';

const cookies = new Cookies();

const makeComposeEnhancer = () => {
  return (
    (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})) ||
    compose
  );
};

const reduxSagaMonitorOptions = {};

const preloadedState = () => {
  const preloadAuth = {
    token: cookies.get('token') || '',
    currentUser: cookies.get('userInfo') || null,
  };

  return {
    auth: preloadAuth,
  };
};

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware(reduxSagaMonitorOptions);
  const middlewares = [sagaMiddleware];
  const store = createStore(
    makeRootReducer(),
    preloadedState(),
    makeComposeEnhancer()(applyMiddleware(...middlewares)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
};

export default configureStore;
