import { all } from 'redux-saga/effects';

import { Sagas as AuthSagas } from './reducers/auth';

export default function* rootSaga() {
  yield all([AuthSagas()]);
}
