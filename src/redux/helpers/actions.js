import { put, call, delay } from 'redux-saga/effects';
import { identity, path, propOr } from 'ramda';

export const pendingAction = type => `${type}_PENDING`;
export const errorAction = type => `${type}_ERROR`;
export const successAction = type => `${type}_SUCCESS`;
export const completeAction = type => `${type}_COMPLETE`;

const noop = () => null;

export const defaultCallback = {
  _success: noop,
  _error: noop,
  _complete: noop,
};

export const getCallback = callback => {
  const _success = propOr(noop, '_success', callback);
  const _error = propOr(noop, '_error', callback);
  const _complete = propOr(noop, '_complete', callback);

  return {
    _success,
    _error,
    _complete,
  };
};

export const getEntity = entity => {
  const pending = propOr(identity, 'pending', entity);
  const success = propOr(identity, 'success', entity);
  const error = propOr(
    error => (error instanceof Error ? error.message : error),
    'error',
    entity,
  );
  const complete = propOr(noop, 'complete', entity);

  return {
    pending,
    success,
    error,
    complete,
  };
};

export const getNormalize = normalize => {
  const success = propOr(noop, 'success', normalize);
  const error = propOr(noop, 'error', normalize);

  return {
    success,
    error,
  };
};

export const entity = (type, entityFn) => {
  const {
    pending: pendingFn,
    success: successFn,
    error: errorFn,
    complete: completeFn,
  } = getEntity(entityFn);

  return {
    pending: data => ({
      type: pendingAction(type),
      payload: pendingFn(data),
    }),

    success: data => ({
      type: successAction(type),
      payload: successFn(data),
    }),

    error: error => ({
      type: errorAction(type),
      errors: errorFn(error),
    }),

    complete: () => ({
      type: completeAction(type),
      payload: completeFn(),
    }),
  };
};

export function fetchEntity({
  request,
  entity,
  normalize,
  callback,
  additional = [],
  isDev = false,
}) {
  return function* makeACall(...data) {
    const { _success, _error, _complete } = getCallback(callback);
    const entityAction = getEntity(entity);
    const normalizeAction = getNormalize(normalize);

    try {
      yield put(entityAction.pending([...additional, ...data]));
      const response = isDev ? yield delay(2000) : yield call(request, ...data);
      const normalized = normalizeAction.success(
        path(['data'], response),
        ...additional,
        ...data,
      );
      yield put(entityAction.success(normalized));
      const retvl = {
        normalized,
        response,
      };
      _success(retvl);
      return retvl;
    } catch (err) {
      yield put(entityAction.error(err));
      _error(err);
      throw err;
    } finally {
      yield put(entityAction.complete());
      _complete();
    }
  };
}
