import { takeEvery, call } from 'redux-saga/effects';
import * as AuthTypes from './actionTypes';
import * as AuthActions from './actions';
import * as AuthRequest from '@/api/AuthRequest';
import * as AuthNormalize from './normalize';
import { fetchEntity } from '@/redux/helpers';
import * as AuthHelper from '@/helpers/AuthHelper';

export function* login(data, callback) {
  const fetchLogin = fetchEntity({
    request: AuthRequest.login,
    entity: AuthActions.loginEntity,
    normalize: AuthNormalize.login,
    callback,
  });

  return yield call(fetchLogin, data);
}

function* workerLogin(action) {
  const { payload, callback } = action;

  try {
    const { normalized } = yield call(login, payload.data, callback);
    yield call(AuthHelper.storeAuthToken, normalized);
  } catch (err) {
    console.error(err);
  }
}

// ------------------------------------------------------------------------------------------------------------------ //

export default function* watchSaga() {
  yield takeEvery(AuthTypes.LOGIN, workerLogin);
}
