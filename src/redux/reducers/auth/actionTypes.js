export const prefix = 'actions.auth';

export const LOGIN = `${prefix}.LOGIN`;
export const LOGOUT = `${prefix}.LOGOUT`;
export const REGISTER = `${prefix}.REGISTER`;
