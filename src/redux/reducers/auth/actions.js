import { entity } from '@/redux/helpers/actions';
import * as AuthTypes from './actionTypes';

export const login = (data, callback) => ({
  type: AuthTypes.LOGIN,
  payload: {
    data,
  },
  callback,
});
export const loginEntity = entity(AuthTypes.LOGIN);

// ------------------------------------------------------------------------------------------------------------------ //

export const logout = callback => ({
  type: AuthTypes.LOGOUT,
  payload: {},
  callback,
});
export const logoutEntity = entity(AuthTypes.LOGOUT);
