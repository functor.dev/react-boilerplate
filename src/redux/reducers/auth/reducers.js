import { combineReducers } from 'redux';
import { errorAction, successAction } from '@/redux/helpers/actions';
import * as AuthTypes from './actionTypes';

export const INITIAL_STATE = {
  token: null,
  currentUser: null,
};

const token = (state = INITIAL_STATE.token, { type, payload }) => {
  switch (type) {
    case successAction(AuthTypes.REGISTER):
    case successAction(AuthTypes.LOGIN):
      return payload.token;

    case successAction(AuthTypes.LOGOUT):
    case errorAction(AuthTypes.LOGOUT):
      return INITIAL_STATE.token;

    default:
      return state;
  }
};

const currentUser = (state = INITIAL_STATE.currentUser, { type, payload }) => {
  switch (type) {
    case successAction(AuthTypes.LOGIN):
    case successAction(AuthTypes.REGISTER): {
      return payload.result;
    }

    case successAction(AuthTypes.LOGOUT):
    case errorAction(AuthTypes.LOGOUT):
      return INITIAL_STATE.currentUser;

    default:
      return state;
  }
};

export default combineReducers({
  token,
  currentUser,
});
