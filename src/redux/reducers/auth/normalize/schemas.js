import { schema } from 'normalizr';

export const user = new schema.Entity('user');

export const registerInfo = new schema.Entity('registerInfo', {
  user,
});
