import { normalize } from 'normalizr';
import { registerInfo } from './schemas';

export const register = {
  success: data => {
    return {
      token: data.token,
      ...normalize(data.user, registerInfo),
    };
  },
};

export const login = {
  success: data => {
    return {
      token: data.token,
      ...normalize(data.user, registerInfo),
    };
  },
};
