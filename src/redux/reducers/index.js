import { combineReducers } from 'redux';
import auth from './auth';

const makeReducers = () =>
  combineReducers({
    auth,
  });

export default makeReducers;
