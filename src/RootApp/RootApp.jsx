import React from 'react';
import PropTypes from 'prop-types';

// Import antd css
import 'antd/dist/antd.css';

import WebFont from 'webfontloader';
import GlobalStyles from '@/styles/GlobalStyles';
import { renderRoutes } from 'react-router-config';

WebFont.load({
  google: {
    families: ['Questrial'],
  },
});

const RootApp = ({ route }) => {
  return (
    <>
      <GlobalStyles />

      {renderRoutes(route.routes)}
    </>
  );
};

RootApp.propTypes = {
  route: PropTypes.object.isRequired,
};

export default RootApp;
