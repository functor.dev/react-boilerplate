import { Redirect } from 'react-router-dom';

import RootApp from './RootApp';

import AuthLayout from '@/layouts/AuthLayout';

import LoginPage from '@/modules/Login';
import RegisterPage from '@/modules/Register';

const routes = [
  {
    component: RootApp,
    routes: [
      {
        path: '/auth',
        component: AuthLayout,
        routes: [
          {
            path: '/auth/login',
            exact: true,
            component: LoginPage,
          },

          {
            path: '/auth/register',
            exact: true,
            component: RegisterPage,
          },

          {
            component: () => <Redirect to="/errors/error-404" />,
          },
        ],
      },
    ],
  },
];

export default routes;
