const fs = require('fs');
const path = require('path');

const prettierOptions = JSON.parse(
  fs.readFileSync(path.resolve(__dirname, '.prettierrc'), 'utf8'),
);

module.exports = {
  extends: ['react-app', 'prettier'],
  plugins: ['prettier', 'react-hooks'],
  rules: {
    'prettier/prettier': ['error', prettierOptions],
    'react/prop-types': 'error',
    'react/default-props-match-prop-types': 'error',
    'react/require-default-props': 'warn',
    'react/jsx-no-undef': [2, { allowGlobals: true }],
    'no-unused-vars': 'error',
  },
  overrides: [
    {
      files: ['**/*.js?(x)'],
      rules: { 'prettier/prettier': ['warn', prettierOptions] },
    },
  ],
};
