const CracoLessPlugin = require('craco-less');
const CracoAlias = require('craco-alias');
const { getThemeVariables } = require('antd/dist/theme');

module.exports = {
  eslint: {
    enable: false,
  },

  babel: {
    loaderOptions: {
      babelrc: true,
    },
  },

  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: 'options',
        aliases: {
          '@/reducers': './src/redux/reducers',
          '@': './src',
        },
      },
    },

    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: getThemeVariables({
              // dark: true,
              // compact: true,
            }),
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
